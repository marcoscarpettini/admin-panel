import { User } from "../types/apiTypes";
import { RootState } from "./store";

export const selectUsers = (state: RootState): User[] | null =>
    state.users;