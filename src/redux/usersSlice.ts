import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { User } from "../types/apiTypes"

export interface IUserState { users: null | User[]; }
const initialState: IUserState = { users: null };

export const usersSlice = createSlice({
    name: "users",
    initialState,
    reducers: {
        setUsersLocal: (state, action: PayloadAction<User[]>) => {
            state.users = action.payload;
        },
        insertUserLocal: (state, action: PayloadAction<User>) => {
            if(state.users)
                state.users = [ ...state.users, action.payload];
            else
                state.users = [ action.payload ];
        },
        updateUserLocal: (state, action: PayloadAction<User>) => {
            let filteredList = state.users?.filter(user => user.id !== action.payload.id);

            if(filteredList && filteredList.length > 1)
                state.users = [ ...filteredList, action.payload ];
            else
                state.users = [ action.payload ];
        },
        deleteUserLocal: (state, action: PayloadAction<number>) => {
            if(state.users)
                state.users = state.users?.filter(user => user.id !== action.payload);
            else
                state.users = [];
        }
    },
});

export const {
    updateUserLocal,
    deleteUserLocal,
} = usersSlice.actions;

export default usersSlice.reducer;