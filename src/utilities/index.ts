interface CatchApiCallParams {
    callback: () => Promise<void>;
    onFailure?: () => void;
    onFinally?: () => void;
}

export const catchApiCall = async ({
    callback,
    onFailure,
    onFinally,
}: CatchApiCallParams): Promise<void> => {
    try {
        await callback();
    } catch (exception: any) {
        console.log(exception);
        if(onFailure instanceof Function) 
            onFailure();
    } finally {
        if(onFinally instanceof Function)
            onFinally();
    }
};