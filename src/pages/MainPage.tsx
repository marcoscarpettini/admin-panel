import { useState, useEffect } from "react";
import { DataGrid, GridCellParams, GridColDef, GridValueGetterParams } from "@mui/x-data-grid";
import { Button, Grid, IconButton } from "@mui/material";
import { User } from "../types/apiTypes";
import { catchApiCall } from "../utilities";
import { deleteUser, getAllUsers } from "../axios/api";
import { useAppDispatch } from "../hooks/reduxHooks";
import { usersSlice } from "../redux/usersSlice";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from '@mui/icons-material/Delete';

function MainPage(): JSX.Element {
    const [users, setUsers] = useState<User[]>([]);
    const [selectedUserId, setSelectedUserId] = useState<number | null>(null);
    const [isNew, setIsNew] = useState<boolean>(true);
    const [open, setOpen] = useState<boolean>(false);
    
    const dispatch = useAppDispatch();

    useEffect(() => {
        catchApiCall({
            callback: async () => {
                const { data } = await getAllUsers();
                setUsers(data);
            },
            onFinally: () => {
                if(users.length > 0)
                    dispatch(usersSlice.actions.setUsersLocal(users));
            }
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleEdit = (userId: number) => {
        setSelectedUserId(userId);
        setIsNew(false);
        setOpen(true);
        console.log(selectedUserId);
        console.log(isNew);
        console.log(open);
    };

    const handleDelete = (userId: number) => {
        catchApiCall({
            callback: async () => {
                await deleteUser(userId);
            },
            onFinally: () => dispatch(usersSlice.actions.deleteUserLocal(userId)),
        })
    }

    const columns: GridColDef[] = [
        { 
            field: "id", 
            headerName: "ID", 
            width: 70,
        },
        { 
            field: "name", 
            headerName: "Name", 
            flex: 4,
        },
        { 
            field: "username", 
            headerName: "Username", 
            flex: 3,
        },
        { 
            field: "city", 
            headerName: "City",
            valueGetter: (params: GridValueGetterParams) => params.row.address.city, 
            flex: 3,
        },
        {
            field: "email",
            headerName: "Email",
            flex: 4,
        },
        {
            field: "edit",
            headerName: "Edit",
            renderCell: (params: GridCellParams) => (
                <IconButton
                    onClick={() => handleEdit(Number(params.row.id))}
                >
                    <EditIcon />
                </IconButton>
            ),
            width: 100,
        },
        {
            field: "delete",
            headerName: "Delete",
            renderCell: (params: GridCellParams) => (
                <IconButton
                    onClick={() => handleDelete(Number(params.row.id))}
                >
                    <DeleteIcon />
                </IconButton>
            ),
            width: 100,
        }
    ];
    return (
        <Grid container style={{padding: "5rem"}}>
            <Grid item xs={9}>
                <h1>Dashboard</h1>
            </Grid>
            <Grid item xs={3} style={{display: "flex", justifyContent: "center", alignItems: "flex-end"}}>
                <Button variant="contained" style={{marginBottom: "1rem"}}>New</Button>
            </Grid>
            <Grid item xs={12} style={{height: "70vh"}}>
                <DataGrid
                    rows={users}
                    columns={columns}
                />
            </Grid>
        </Grid>
    );
};

export default MainPage