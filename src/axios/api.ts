import axios from "./axios";
import { AxiosResponse } from "axios";
import { User } from "../types/apiTypes";

export const getAllUsers = async ():Promise<AxiosResponse<User[]>> =>
    axios.get("/data");

export const insertUser = async (
    user: User
): Promise<AxiosResponse<User>> => axios.post("/data", user);

export const updateUser = async (
    user: User
): Promise<AxiosResponse<User>> => axios.put("/data", user);

export const deleteUser = async (
    userId: number
): Promise<AxiosResponse<void>> => axios.delete(`/data/${userId}`);