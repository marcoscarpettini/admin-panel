import Axios from "axios";

const instance = Axios.create({
    baseURL: "https://my-json-server.typicode.com/karolkproexe/jsonplaceholderdb",
});

export default instance;